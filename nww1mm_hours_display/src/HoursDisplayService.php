<?php

namespace Drupal\nww1mm_hours_display;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class HoursDisplayService.
 */
class HoursDisplayService {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Constructs a new HoursDisplayService object.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('nww1mm_hours_display.hoursdisplaysettings');
  }

  /**
   * Return the block content based on configuration and current date.
   *
   * @return array
   *   Return the block content based on configuration and current date.
   */
  public function getHoursBlockContent() {
    $current_time = \Drupal::time()
      ->getCurrentTime();
    $current_day = \Drupal::service('date.formatter')->format($current_time, 'custom', 'l');
    $current_month_day = \Drupal::service('date.formatter')->format($current_time, 'custom', 'm/d');
    $closed_dates = explode(',', $this->config->get('closed_dates'));
    $block_content = [];
    if (in_array($current_month_day, $closed_dates) || $this->config->get(strtolower($current_day)) == NULL) {
      // Return values if closed date or closed if empty value for current day.
      $block_content['status'] = "closed";
      $block_content['message'] = $this->config->get('closed_message');
      return $block_content;
    }
    // Implement open day.
    $block_content['status'] = "open";
    $block_content['message'] = $this->config->get('open_message') . " " . $this->config->get(strtolower($current_day));

    return $block_content;
  }

}
