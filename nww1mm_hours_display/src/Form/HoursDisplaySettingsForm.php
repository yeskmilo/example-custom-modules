<?php

namespace Drupal\nww1mm_hours_display\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class HoursDisplaySettingsForm.
 */
class HoursDisplaySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nww1mm_hours_display.hoursdisplaysettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hours_display_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nww1mm_hours_display.hoursdisplaysettings');
    $form['days_wrapper_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Days of the week'),
      '#prefix' => '<div id="days-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['days_wrapper_fieldset']['monday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Monday'),
      '#description' => $this->t('Input open hours for monday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('monday'),
    ];
    $form['days_wrapper_fieldset']['tuesday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tuesday'),
      '#description' => $this->t('Input open hours for tuesday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('tuesday'),
    ];
    $form['days_wrapper_fieldset']['wednesday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wednesday'),
      '#description' => $this->t('Input open hours for wednesday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('wednesday'),
    ];
    $form['days_wrapper_fieldset']['thursday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Thursday'),
      '#description' => $this->t('Input open hours for thursday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('thursday'),
    ];
    $form['days_wrapper_fieldset']['friday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Friday'),
      '#description' => $this->t('Input open hours for friday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('friday'),
    ];
    $form['days_wrapper_fieldset']['saturday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Saturday'),
      '#description' => $this->t('Input open hours for saturday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('saturday'),
    ];
    $form['days_wrapper_fieldset']['sunday'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sunday'),
      '#description' => $this->t('Input open hours for sunday'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('sunday'),
    ];
    $form['open_closed_wrapper_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Open - Closed settings'),
      '#prefix' => '<div id="open-closed-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['open_closed_wrapper_fieldset']['closed_dates'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Closed dates'),
      '#description' => $this->t('A comma separated list of dates (e.g. "12/25") that the museum will be closed.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('closed_dates'),
    ];
    $form['open_closed_wrapper_fieldset']['open_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Open message'),
      '#description' => $this->t('The text that will appear before hours when museum is open. (e.g. "Open today:")'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('open_message'),
    ];
    $form['open_closed_wrapper_fieldset']['closed_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Closed message'),
      '#description' => $this->t('The text that will appear when museum is closed. (e.g. "Closed today")'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('closed_message'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('nww1mm_hours_display.hoursdisplaysettings')
      ->set('monday', $form_state->getValue('monday'))
      ->set('tuesday', $form_state->getValue('tuesday'))
      ->set('wednesday', $form_state->getValue('wednesday'))
      ->set('thursday', $form_state->getValue('thursday'))
      ->set('friday', $form_state->getValue('friday'))
      ->set('saturday', $form_state->getValue('saturday'))
      ->set('sunday', $form_state->getValue('sunday'))
      ->set('closed_dates', $form_state->getValue('closed_dates'))
      ->set('open_message', $form_state->getValue('open_message'))
      ->set('closed_message', $form_state->getValue('closed_message'))
      ->save();
  }

}
