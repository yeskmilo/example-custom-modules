<?php

namespace Drupal\nww1mm_hours_display\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'HoursDisplayBlock' block.
 *
 * @Block(
 *  id = "hours_display_block",
 *  admin_label = @Translation("Hours display block"),
 * )
 */
class HoursDisplayBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $hours_block_content = \Drupal::service('nww1mm_hours_display.service')->getHoursBlockContent();
    $build = [];
    $build['#theme'] = 'hours_display_block';
    $build['#markup'] = 'Implement HoursDisplayBlock.';
    $build['#cache']['max-age'] = 0;
    $build['#status'] = $hours_block_content['status'];
    $build['#message'] = $hours_block_content['message'];

    return $build;
  }

  /**
   * Set cache to 0.
   *
   * @return int
   *   Return cache value 0
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
