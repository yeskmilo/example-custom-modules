<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ztv_subscription\Entity\SubscriptionEntityInterface;

/**
 * Defines the storage handler class for Subscription entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Subscription entity entities.
 *
 * @ingroup ztv_subscription
 */
interface SubscriptionEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Subscription entity revision IDs for a specific Subscription entity.
   *
   * @param \Drupal\ztv_subscription\Entity\SubscriptionEntityInterface $entity
   *   The Subscription entity entity.
   *
   * @return int[]
   *   Subscription entity revision IDs (in ascending order).
   */
  public function revisionIds(SubscriptionEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Subscription entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Subscription entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

}
