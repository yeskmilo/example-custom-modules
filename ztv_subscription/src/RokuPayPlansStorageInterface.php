<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\ztv_subscription\Entity\RokuPayPlansInterface;

/**
 * Defines the storage handler class for Roku pay plans entities.
 *
 * This extends the base storage class, adding required special handling for
 * Roku pay plans entities.
 *
 * @ingroup ztv_subscription
 */
interface RokuPayPlansStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Roku pay plans revision IDs for a specific Roku pay plans.
   *
   * @param \Drupal\ztv_subscription\Entity\RokuPayPlansInterface $entity
   *   The Roku pay plans entity.
   *
   * @return int[]
   *   Roku pay plans revision IDs (in ascending order).
   */
  public function revisionIds(RokuPayPlansInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Roku pay plans author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Roku pay plans revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\ztv_subscription\Entity\RokuPayPlansInterface $entity
   *   The Roku pay plans entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(RokuPayPlansInterface $entity);

  /**
   * Unsets the language for all Roku pay plans with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
