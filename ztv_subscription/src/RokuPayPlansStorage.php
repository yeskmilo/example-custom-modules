<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\ztv_subscription\Entity\RokuPayPlansInterface;

/**
 * Defines the storage handler class for Roku pay plans entities.
 *
 * This extends the base storage class, adding required special handling for
 * Roku pay plans entities.
 *
 * @ingroup ztv_subscription
 */
class RokuPayPlansStorage extends SqlContentEntityStorage implements RokuPayPlansStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(RokuPayPlansInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {roku_pay_plans_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {roku_pay_plans_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(RokuPayPlansInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {roku_pay_plans_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('roku_pay_plans_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
