<?php

namespace Drupal\ztv_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SubscriptionFiltersForm.
 */
class SubscriptionFiltersForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'subscription_filters_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
    $form['filters'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('form--inline clearfix')),
    ];
    $form['filters']['transaction_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Transaction Date'),
      '#weight' => '0',
      '#default_value' => isset($request['transaction_date']) ? $request['transaction_date'] : '',
    ];
    $form['filters']['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email'),
      '#maxlength' => 64,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => isset($request['email']) ? $request['email'] : '',
    ];
    $values = $this->getTransactionTypeOptions();
    $form['filters']['transaction_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Transaction type'),
      '#options' => $values,
      '#size' => 1,
      '#weight' => '0',
      '#default_value' => isset($request['transaction_type']) ? $request['transaction_type'] : 'any',
    ];
    $form['actions'] = [
      '#type' => 'container',
      '#attributes' => array('class' => array('form-actions')),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
    ];
    $form['actions']['reset'] = array
    (
      '#type' => 'submit',
      '#value' => $this->t('Reset Filters'),
      '#submit' => array([$this, 'resetFilters']),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  public function resetFilters(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.subscription_entity.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $email = $form_state->getValue('email');
    $tDate = $form_state->getValue('transaction_date');
    $tType = $form_state->getValue('transaction_type');
    if($email) {
      $query['email'] = $email;
    }
    if($tDate) {
      $query['transaction_date'] = $tDate;
    }
    if($tType) {
      $query['transaction_type'] = $tType;
    }
    $form_state->setRedirect('entity.subscription_entity.collection', $query);
  }

  public function getTransactionTypeOptions() {
    $connection = \Drupal::database();
    $query = $connection->select('subscription_entity', 'se');
    $query->fields('se', ['transactionType']);
    $results = $query->distinct()->execute()->fetchAll();
    $values['any'] = 'Any';
    foreach($results as $value) {
      $values[$value->transactionType] = $value->transactionType;
    }
    return $values;
  }


}
