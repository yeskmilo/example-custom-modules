<?php

namespace Drupal\ztv_subscription\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Roku pay plans revision.
 *
 * @ingroup ztv_subscription
 */
class RokuPayPlansRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Roku pay plans revision.
   *
   * @var \Drupal\ztv_subscription\Entity\RokuPayPlansInterface
   */
  protected $revision;

  /**
   * The Roku pay plans storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $rokuPayPlansStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->rokuPayPlansStorage = $container->get('entity_type.manager')->getStorage('roku_pay_plans');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'roku_pay_plans_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.roku_pay_plans.version_history', ['roku_pay_plans' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $roku_pay_plans_revision = NULL) {
    $this->revision = $this->rokuPayPlansStorage->loadRevision($roku_pay_plans_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->rokuPayPlansStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Roku pay plans: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Roku pay plans %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.roku_pay_plans.canonical',
       ['roku_pay_plans' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {roku_pay_plans_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.roku_pay_plans.version_history',
         ['roku_pay_plans' => $this->revision->id()]
      );
    }
  }

}
