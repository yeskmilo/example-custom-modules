<?php

namespace Drupal\ztv_subscription\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RokuApiSettingsForm.
 */
class RokuApiSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ztv_subscription.rokuapisettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'roku_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ztv_subscription.rokuapisettings');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('The Roku API key that will be used on push notifications response header'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ztv_subscription.rokuapisettings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
  }

}
