<?php

namespace Drupal\ztv_subscription\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Roku pay plans entities.
 *
 * @ingroup ztv_subscription
 */
class RokuPayPlansDeleteForm extends ContentEntityDeleteForm {


}
