<?php

namespace Drupal\ztv_subscription\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Subscription entity entities.
 *
 * @ingroup ztv_subscription
 */
class SubscriptionEntityDeleteForm extends ContentEntityDeleteForm {


}
