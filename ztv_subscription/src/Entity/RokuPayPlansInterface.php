<?php

namespace Drupal\ztv_subscription\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Roku pay plans entities.
 *
 * @ingroup ztv_subscription
 */
interface RokuPayPlansInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Roku pay plans name.
   *
   * @return string
   *   Name of the Roku pay plans.
   */
  public function getName();

  /**
   * Sets the Roku pay plans name.
   *
   * @param string $name
   *   The Roku pay plans name.
   *
   * @return \Drupal\ztv_subscription\Entity\RokuPayPlansInterface
   *   The called Roku pay plans entity.
   */
  public function setName($name);

  /**
   * Gets the Roku pay plans creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Roku pay plans.
   */
  public function getCreatedTime();

  /**
   * Sets the Roku pay plans creation timestamp.
   *
   * @param int $timestamp
   *   The Roku pay plans creation timestamp.
   *
   * @return \Drupal\ztv_subscription\Entity\RokuPayPlansInterface
   *   The called Roku pay plans entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Roku pay plans revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Roku pay plans revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\ztv_subscription\Entity\RokuPayPlansInterface
   *   The called Roku pay plans entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Roku pay plans revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Roku pay plans revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\ztv_subscription\Entity\RokuPayPlansInterface
   *   The called Roku pay plans entity.
   */
  public function setRevisionUserId($uid);

}
