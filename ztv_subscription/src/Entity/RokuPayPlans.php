<?php

namespace Drupal\ztv_subscription\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Roku pay plans entity.
 *
 * @ingroup ztv_subscription
 *
 * @ContentEntityType(
 *   id = "roku_pay_plans",
 *   label = @Translation("Roku pay plans"),
 *   handlers = {
 *     "storage" = "Drupal\ztv_subscription\RokuPayPlansStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ztv_subscription\RokuPayPlansListBuilder",
 *     "views_data" = "Drupal\ztv_subscription\Entity\RokuPayPlansViewsData",
 *     "translation" = "Drupal\ztv_subscription\RokuPayPlansTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\ztv_subscription\Form\RokuPayPlansForm",
 *       "add" = "Drupal\ztv_subscription\Form\RokuPayPlansForm",
 *       "edit" = "Drupal\ztv_subscription\Form\RokuPayPlansForm",
 *       "delete" = "Drupal\ztv_subscription\Form\RokuPayPlansDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\ztv_subscription\RokuPayPlansHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ztv_subscription\RokuPayPlansAccessControlHandler",
 *   },
 *   base_table = "roku_pay_plans",
 *   data_table = "roku_pay_plans_field_data",
 *   revision_table = "roku_pay_plans_revision",
 *   revision_data_table = "roku_pay_plans_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer roku pay plans entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}",
 *     "add-form" = "/admin/ztv_content/roku_pay_plans/add",
 *     "edit-form" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/edit",
 *     "delete-form" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/delete",
 *     "version-history" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/revisions",
 *     "revision" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/revisions/{roku_pay_plans_revision}/view",
 *     "revision_revert" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/revisions/{roku_pay_plans_revision}/revert",
 *     "revision_delete" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/revisions/{roku_pay_plans_revision}/delete",
 *     "translation_revert" = "/admin/ztv_content/roku_pay_plans/{roku_pay_plans}/revisions/{roku_pay_plans_revision}/revert/{langcode}",
 *     "collection" = "/admin/ztv_content/roku_pay_plans",
 *   },
 *   field_ui_base_route = "roku_pay_plans.settings"
 * )
 */
class RokuPayPlans extends EditorialContentEntityBase implements RokuPayPlansInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly,
    // make the roku_pay_plans owner the revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Roku pay plans entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Roku pay plan.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(TRUE)
      ->setRequired(TRUE);

    $fields['product_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Product ID'))
      ->setDescription(t('Unique Product identifier.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 100,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setTranslatable(TRUE)
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Current Plan Details.'))
      ->setCardinality(2)
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 65,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE)
      ->setTranslatable(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Roku pay plans is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 10,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
