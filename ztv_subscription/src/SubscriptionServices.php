<?php


namespace Drupal\ztv_subscription;


use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

class SubscriptionServices {

  protected $entityTypeManager;
  protected $subscriptionStorage;

  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->subscriptionStorage = $entityTypeManager->getStorage('subscription_entity');
  }

  public function setSubscription($bodySubscription) {
    $subscription =  $this->getSubscriptionByCustomerId($bodySubscription['customerId']);
    if(!empty($subscription)) {
      $subscription = reset($subscription);
      //Assign new values to subscription entity
      foreach ($bodySubscription as $key => $value) {
        if (preg_match('/Date/i', $key)){
          $date = new DrupalDateTime($value, 'UTC');
          $value = \Drupal::service('date.formatter')->format($date->getTimestamp(), 'custom', 'Y-m-d\TH:i:s');
        }
          $subscription->set($key,$value);
      }
      $subscription->save();
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  public function getSubscriptionByCustomerId($customerId) {
    $params = ['customerId' => $customerId];

    $entities = $this->subscriptionStorage->loadByProperties($params);
    if (empty($entities)){
      return [];
    }

    return $entities;
  }

}
