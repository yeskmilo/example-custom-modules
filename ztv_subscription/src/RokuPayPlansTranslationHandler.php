<?php

namespace Drupal\ztv_subscription;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for roku_pay_plans.
 */
class RokuPayPlansTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
