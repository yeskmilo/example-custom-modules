<?php

namespace Drupal\ztv_subscription\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\ztv_subscription\Entity\RokuPayPlansInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class RokuPayPlansController.
 *
 *  Returns responses for Roku pay plans routes.
 */
class RokuPayPlansController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Roku pay plans revision.
   *
   * @param int $roku_pay_plans_revision
   *   The Roku pay plans revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($roku_pay_plans_revision) {
    $roku_pay_plans = $this->entityTypeManager()->getStorage('roku_pay_plans')
      ->loadRevision($roku_pay_plans_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('roku_pay_plans');

    return $view_builder->view($roku_pay_plans);
  }

  /**
   * Page title callback for a Roku pay plans revision.
   *
   * @param int $roku_pay_plans_revision
   *   The Roku pay plans revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($roku_pay_plans_revision) {
    $roku_pay_plans = $this->entityTypeManager()->getStorage('roku_pay_plans')
      ->loadRevision($roku_pay_plans_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $roku_pay_plans->label(),
      '%date' => $this->dateFormatter->format($roku_pay_plans->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Roku pay plans.
   *
   * @param \Drupal\ztv_subscription\Entity\RokuPayPlansInterface $roku_pay_plans
   *   A Roku pay plans object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(RokuPayPlansInterface $roku_pay_plans) {
    $account = $this->currentUser();
    $roku_pay_plans_storage = $this->entityTypeManager()->getStorage('roku_pay_plans');

    $langcode = $roku_pay_plans->language()->getId();
    $langname = $roku_pay_plans->language()->getName();
    $languages = $roku_pay_plans->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $roku_pay_plans->label()]) : $this->t('Revisions for %title', ['%title' => $roku_pay_plans->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all roku pay plans revisions") || $account->hasPermission('administer roku pay plans entities')));
    $delete_permission = (($account->hasPermission("delete all roku pay plans revisions") || $account->hasPermission('administer roku pay plans entities')));

    $rows = [];

    $vids = $roku_pay_plans_storage->revisionIds($roku_pay_plans);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\ztv_subscription\RokuPayPlansInterface $revision */
      $revision = $roku_pay_plans_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $roku_pay_plans->getRevisionId()) {
          $link = $this->l($date, new Url('entity.roku_pay_plans.revision', [
            'roku_pay_plans' => $roku_pay_plans->id(),
            'roku_pay_plans_revision' => $vid,
          ]));
        }
        else {
          $link = $roku_pay_plans->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.roku_pay_plans.translation_revert', [
                'roku_pay_plans' => $roku_pay_plans->id(),
                'roku_pay_plans_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.roku_pay_plans.revision_revert', [
                'roku_pay_plans' => $roku_pay_plans->id(),
                'roku_pay_plans_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.roku_pay_plans.revision_delete', [
                'roku_pay_plans' => $roku_pay_plans->id(),
                'roku_pay_plans_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['roku_pay_plans_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
