<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Roku pay plans entities.
 *
 * @ingroup ztv_subscription
 */
class RokuPayPlansListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['product_id'] = $this->t('Product ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\ztv_subscription\Entity\RokuPayPlans $entity */
    $row['product_id'] = $entity->get('product_id')->getString();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.roku_pay_plans.edit_form',
      ['roku_pay_plans' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
