<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Roku pay plans entity.
 *
 * @see \Drupal\ztv_subscription\Entity\RokuPayPlans.
 */
class RokuPayPlansAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\ztv_subscription\Entity\RokuPayPlansInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished roku pay plans entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published roku pay plans entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit roku pay plans entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete roku pay plans entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add roku pay plans entities');
  }


}
