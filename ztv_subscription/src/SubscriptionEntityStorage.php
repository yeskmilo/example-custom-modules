<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\ztv_subscription\Entity\SubscriptionEntityInterface;

/**
 * Defines the storage handler class for Subscription entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Subscription entity entities.
 *
 * @ingroup ztv_subscription
 */
class SubscriptionEntityStorage extends SqlContentEntityStorage implements SubscriptionEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(SubscriptionEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {subscription_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {subscription_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

}
