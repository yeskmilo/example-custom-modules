<?php

namespace Drupal\ztv_subscription;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\user\Entity\User;

/**
 * Defines a class to build a listing of Subscription entity entities.
 *
 * @ingroup ztv_subscription
 */
class SubscriptionEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Subscription ID');
    $header['customerId'] = $this->t('Customer ID');
    $header['productName'] = $this->t('Product Name');
    $header['transactionType'] = $this->t('Transaction Type');
    $header['transactionDate'] = $this->t('Transaction date');
    $header['user'] = $this->t('User');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\ztv_subscription\Entity\SubscriptionEntity $entity */
    $row['id'] = $entity->id();
    $row['customerId'] = Link::createFromRoute(
      $entity->label(),
      'entity.subscription_entity.edit_form',
      ['subscription_entity' => $entity->id()]
    );
    $row['productName'] = $entity->get('productName')->getString();
    $row['transactionType'] = $entity->get('transactionType')->getString();
    $row['transactionDate'] = date("D d/m/Y", $entity->get('changed')->getString());
    $userId = $entity->get('user_id')->getString();
    $account = User::load($userId);
    if ($account) {
      $row['user'] = Link::createFromRoute(
        $account->getEmail(),
        'entity.user.edit_form',
        ['user' => $userId]
      );
    }
    else {
      $row['user'] = '- Inactive Account -';
    }
    return $row + parent::buildRow($entity);
  }
  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\ztv_subscription\Form\SubscriptionFiltersForm');
    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
    if(empty($request)) {
      parent::getEntityIds();
    }
    else {
      $query = \Drupal::entityQuery($this->entityTypeId);
      if(isset($request['email']) && !empty($request['email'])) {
        $userId = $this->getUserIdByEmail($request['email']);
        $query->condition('user_id', $userId);

      }
      if(isset($request['transaction_type']) && !empty($request['transaction_type']) && $request['transaction_type'] != 'any') {
        $query->condition('transactionType', $request['transaction_type']);
      }
      if(isset($request['transaction_date']) && !empty($request['transaction_date'])) {
        $date = strtotime($request['transaction_date']);
        $query->condition('changed', [$date, ($date+86400)], 'BETWEEN');
      }
      return $query->execute();
    }
  }

  public function getUserIdByEmail($email){
    $userId = NULL;
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $user = user_load_by_mail($email);
      if ($user) {
        $userId = $user->id();
      }
    }
    return $userId;
  }

}
