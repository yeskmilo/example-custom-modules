<?php

namespace Drupal\ztv_subscription\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "push_notification_rest_resource",
 *   label = @Translation("Push notification rest resource"),
 *   uri_paths = {
 *     "create" = "/api/subscription/pushnotification"
 *   }
 * )
 */
class PushNotificationRestResource extends ResourceBase {

  /**
   * @var \Drupal\ztv_subscription\SubscriptionServices
   */
  protected $subscriptionServices;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('ztv_subscription');
    $instance->subscriptionServices = $container->get('subscription.services');
    $instance->subscriptionStorage = $container->get('subscription.storage');
    return $instance;
  }

    /**
     * Responds to POST requests.
     *
     * @param string $payload
     *
     * @return \Drupal\rest\ModifiedResourceResponse
     *   The HTTP response object.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function post($payload, $request) {

      // TODO DELETE THIS LOG AFTER TEST
      \Drupal::logger("Push notification - Request")
        ->debug('Payload: <pre><code>'
          . print_r($payload, TRUE)
          . '</code></pre>'
          . 'Headers: <pre><code>'
          . print_r($request->headers, TRUE)
          . '</code></pre>');

      $response = [];
      if ( !isset($payload['customerId']) ){
        $response = ['status' => 'ERROR', 'message' => 'customerId was no specified on body request'];
        return new ModifiedResourceResponse($response, 207);
      }
      if ( !isset($payload['transactionType']) ){
        $response = ['status' => 'ERROR', 'message' => 'transactionType was no specified on body request'];
        return new ModifiedResourceResponse($response, 207);
      }
      $response = new Response();
      $subscription_exists = $this->subscriptionServices->setSubscription($payload);

      if(!$subscription_exists) {
        $response = ['status' => 'ERROR', 'message' => 'no subscription found for customerID'];
        return new ModifiedResourceResponse($response, 207);
      }

      $response->headers->set("ApiKey", \Drupal::config('ztv_subscription.rokuapisettings')->get('api_key'));
      $response->headers->set("Content-Length", 32);
      $response->setContent($payload['responseKey']);


      // TODO DELETE THIS LOG AFTER TEST
      \Drupal::logger("Push notification - Response")
        ->debug('Payload: <pre><code>'
          . print_r($payload['responseKey'], TRUE)
          . '</code></pre>'
          . 'Headers: <pre><code>'
          . print_r($response->headers, TRUE)
          . '</code></pre>'
          . 'New response: <pre><code>'
          . print_r($response, TRUE)
          . '</code></pre>');

      return $response;
    }

}
