<?php

namespace Drupal\ztv_subscription\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_subscribe",
 *   label = @Translation("User subscribe"),
 *   uri_paths = {
 *     "create" = "/user/subscribe"
 *   }
 * )
 */
class UserSubscribe extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  protected $subscriptionFieldDefinition, $userFieldDefinition;

  protected $formatDate;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('ztv_subscription');
    $instance->currentUser = $container->get('current_user');
    //$instance->subscriptionEntityStorage = $container->get('entity_type.manager')->getStorage('subscription_entity');
    $instance->subscriptionFieldDefinition = $container->get('entity_field.manager')->getFieldStorageDefinitions("subscription_entity");
    $instance->userFieldDefinition = $container->get('entity_field.manager')->getFieldStorageDefinitions("user");
    $instance->formatDate = $container->get('date.formatter');

    return $instance;
  }

  /**
   * Responds to POST requests.
   *
   * @param string $payload
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post($payload) {

    $entities = [
      'user' => $this->userFieldDefinition,
      'subscription_entity' => $this->subscriptionFieldDefinition
    ];

    // Parse payload values extracting only the expected entity values
    $entities_values = [];

    foreach ($entities as $entity => $fieldDefinition){
      $values = isset($payload[$entity]) ? $payload[$entity] : false;
      if ($values){
        foreach ($fieldDefinition as $field){
          if ( key_exists($field->getName(), $values) ){
            if ( ($field->getType() == 'created') && is_string($values[$field->getName()])){
              $values[$field->getName()] = strtotime($values[$field->getName()]);
            }
            if ( ($field->getType() == 'datetime') && $field->getSetting("datetime_type") && is_string($values[$field->getName()])){
              $date = $this->formatDate->format(strtotime($values[$field->getName()]), "custom", "Y-m-d\\TH:i:s");
              $values[$field->getName()] = $date;
            }
            $entities_values[$entity]['create'][$field->getName()] = $values[$field->getName()];
          } elseif ( $field->getName() !== 'user_id'  && method_exists($field, 'isRequired') && $field->isRequired()) {
            $entities_values[$entity]['missing'][$field->getName()] = $field->getLabel();
          }
        }
      } else {
        return new ModifiedResourceResponse(['error' => "$entity property is missing"], 400);
      }
    }

    // Validate required values
    $errors = [];
    foreach ($entities_values as $entity => $fields){
      if (isset($fields['missing'])){
        $errors[] = $this->formatPlural(count($fields['missing']), "The field @fields is required for entity @entity", "The fields @fields are required for entity @entity", [
          "@fields" => implode(", ", $fields['missing']),
          "@entity" => $entity
        ]);
      }
    }
    if (!empty($errors) ){
      $message = implode(",", $errors);
      return new ModifiedResourceResponse(['error' => $message], 400);
    }

    // Validate entity constraints
    $new_user = \Drupal\user\Entity\User::create($entities_values['user']['create']);

    $entities_values['subscription_entity']['create']['user_id'] = 0;
    $new_subscription = \Drupal\ztv_subscription\Entity\SubscriptionEntity::create($entities_values['subscription_entity']['create']);

    $errors = [];

    foreach ([$new_user->validate(), $new_subscription->validate()] as $violations){
      if ($violations->count() > 0) {
        for ($i = 0; $i < $violations->count(); $i++) {
          $error = $violations->get($i);
          if ($error->getConstraint() instanceof \Drupal\user\Plugin\Validation\Constraint\UserNameUnique){
            continue;
          }
          $errors[] = strip_tags($error->getMessage());
        }
      }
    }

    if (!empty($errors)){
      $message = implode(", ",$errors);
      return new ModifiedResourceResponse(['error' => $message], 400);
    }

    // Save entities
    $new_user->save();
    $new_subscription->set('user_id', $new_user->id());
    $new_subscription->save();

    $response = [
      'status' => 'OK',
      'uid' => (int) $new_user->id(),
      'subscription_entity_id' => (int) $new_subscription->id(),
    ];

    return new ModifiedResourceResponse($response, 200);
  }

}
