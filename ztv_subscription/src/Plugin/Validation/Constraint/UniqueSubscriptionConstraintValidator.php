<?php

namespace Drupal\ztv_subscription\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class UniqueSubscriptionConstraintValidator extends ConstraintValidator
{

    /**
     * {@inheritdoc}
     */
    public function validate($entity, Constraint $constraint) {
      // Next check if the value is unique.
      if (!$this->isUnique($entity)) {
        $this->context->addViolation($constraint->notUnique);
      }
    }

    private function isUnique($entity) {
      if ($entity->isNew()) {
        $query = \Drupal::entityQuery('subscription_entity');
        $group = $query
          ->orConditionGroup()
          ->condition('user_id', $entity->get('user_id')->entity->id())
          ->condition('customerId', $entity->get('customerId')->getString());
        $query
          ->condition($group);
        $storageContent = $query->execute();
        if ($storageContent) {
          return FALSE;
        }
        return TRUE;
      }
      return TRUE;
    }

}
