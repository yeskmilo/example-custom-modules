<?php

namespace Drupal\ztv_subscription\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Plugin implementation of the 'unique_subscription_constraint'.
 *
 * @Constraint(
 *   id = "unique_subscription_constraint",
 *   label = @Translation("Unique subscription constraint", context = "Validation"),
 * )
 */
class UniqueSubscriptionConstraint extends Constraint
{

    // The message that will be shown if the value is not unique.
    public $notUnique = 'The subsciption customerID or User id is not unique';

}
