<?php

/**
 * @file
 * Contains roku_pay_plans.page.inc.
 *
 * Page callback for Roku pay plans entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Roku pay plans templates.
 *
 * Default template: roku_pay_plans.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_roku_pay_plans(array &$variables) {
  // Fetch RokuPayPlans Entity Object.
  $roku_pay_plans = $variables['elements']['#roku_pay_plans'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
