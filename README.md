# README #

Example Drupal custom modules

### What is this repository for? ###

* Show some coding example related to custom module implementations
  * Hours display block module.
    * This module is intended to show a block in some region with open or closed hours.
      * Settings form
      * Block available to be placed in any region.
      * Custom template.
      * Drupal 9 module.
  * Subscription module.
    * This module is intended to create subscription entity, creation form, list existing entities, constraint validations and custom WS implementation.
    * Drupal 8 module

### How do I get set up? ###

* Copy modules into some working drupal installation and try that.
* The hours display block module creates a new block that can be placed on any page region.
* The hours display block expose and admin configuration form to input information per days.